Este proyecto fue creado con [Create React App](https://github.com/facebook/create-react-app).

## Setup y start

* Forkear el proyecto
* Ejecutar `npm install`
* Ejecutar `npm start`
* La aplicación comenzará a ejecutarse bajo `localhost:3000` u otro puerto en el caso que este siendo utilizado
