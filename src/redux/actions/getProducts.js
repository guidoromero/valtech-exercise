export const getProducts = () => dispatch => {
  return fetch('https://demo0928420.mockable.io/products/list')
    .then(response => response.json())
    .then(products => {dispatch({ type: 'FETCH_PRODUCTS', products });
  });
}