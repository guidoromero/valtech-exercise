import { orderAscByName, orderDescByName, orderAscByPrice, orderDescByPrice } from '../../FiltersFunctions';

const initialState = {
  filters:{
    fsearch: '',
    fstock: false,
    fprecio: '',
    falfa: ''
  },
  products:[]
};

const productsReducer = (state=initialState, action) => {
  switch(action.type){
    case 'FETCH_PRODUCTS': return {
      ...state,
      ...action.products
    };
    case 'SET_FILTER': return {
      ...state,
      filters: {
        ...state.filters,
        [action.filter.id]:action.filter.value
      } 
    }
  }
  return state;
}

export const selectProducts = state => {
  let products = state.productsReducer.products.slice()
  
  if(state.productsReducer.filters.fsearch !== '') products = products.filter(product => product.name.includes(state.productsReducer.filters.fsearch));

  if(state.productsReducer.filters.fstock) products = products.filter(product => product.quantity >= 500);  
  
  if(state.productsReducer.filters.falfa === 'ascendente')  products = orderAscByName(products);
  else if(state.productsReducer.filters.falfa === 'descendente') products = orderDescByName(products);
  
  if(state.productsReducer.filters.fprecio === 'ascendente')  products = orderAscByPrice(products);
  else if(state.productsReducer.filters.fprecio === 'descendente') products = orderDescByPrice(products);
  
  return products;
} 

export default productsReducer;