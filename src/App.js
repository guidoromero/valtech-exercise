import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getProducts } from './redux/actions/getProducts';
import { selectProducts } from './redux/reducers/productsReducer';
import Filter from 'components/Filter';
import Products from 'components/Products';

class App extends Component {
  
  constructor(props){
    super(props);
  }

  componentDidMount(){
    this.props.getProducts();
  }
  
  render() {    
    let { products } = this.props;
    return (
      <>
        <Filter />
        <Products products={products}/>
      </>
    )
  }
}

const mapStateToProps = state => {
  return {
    products: selectProducts(state)
  }
}

const mapDispatchToProps = {
  getProducts
}

export default connect(mapStateToProps,mapDispatchToProps)(App);