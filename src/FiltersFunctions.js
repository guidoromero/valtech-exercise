export const orderAscByName = products => {
    return products.slice().sort((a,b)=> {
        a = a.name.toLowerCase();
        b = b.name.toLowerCase();

        return a > b ? 1 : -1;
    });
    
}

export const orderDescByName = products => {
    return products.slice().sort((a,b)=> {
        a = a.name.toLowerCase();
        b = b.name.toLowerCase();

        return a < b ? 1 : -1;
    });
}

export const orderAscByPrice = products => {
    return products.slice().sort((a,b)=> {
        a = +a.price.replace(/\D/g, '');
        b = +b.price.replace(/\D/g, '');
        return a > b ? 1 : -1;
    })
}

export const orderDescByPrice = products => {
    return products.slice().sort((a,b)=> {
        a = a.price.slice(1);
        b = b.price.slice(1);
    
        return a < b ? 1 : -1;
    })
}