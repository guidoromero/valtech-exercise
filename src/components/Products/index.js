import React from 'react'
import Product from 'components/Product';
import Title from 'components/Title';

import { ProductsWrapper, ProductsListWrapper } from './styled'

const Products = ({ products }) => (
  <ProductsWrapper>
    <Title>Lista de productos ({products.length})</Title>
    <ProductsListWrapper>
      {(products || []).map(product => (
        <Product key={product.id} product={product} />
      ))}
    </ProductsListWrapper>
  </ProductsWrapper>
);

export default Products;
