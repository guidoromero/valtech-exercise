import styled from 'styled-components'

export const ProductsWrapper = styled.div`
  justify-content: flex-start;
  flex-wrap: wrap;
  margin: 20px;
`;

export const ProductsListWrapper = styled.div`
  display: grid;
  grid-gap: 20px;
  grid-template-columns: repeat(5, 1fr);
`
