import React from 'react'

import { ProductWrapper, ProductName, ProductInfo, ProductSubline } from './styled'

const Product = ({ product }) => (
  <ProductWrapper>
    <ProductInfo>
      <ProductName>{product.name}</ProductName>
      <ProductSubline>{product.price}</ProductSubline>
      <ProductSubline>{product.available ? 'Disponible' : 'No disponible'}</ProductSubline>
      <ProductSubline>Quedan {product.quantity} en stock</ProductSubline>
    </ProductInfo>
  </ProductWrapper>
);

export default Product;
