import styled from 'styled-components'

export const FiltersWrapper = styled.div`
  display:flex;
  justify-content: center;
  align-items: center;
  flex-direction:column;
  font-family: "Comic Sans MS", cursive, sans-serif;
  background:#2D354C;
  color:#FFFFFF;
  height:200px;
`;

export const Input = styled.input`
  font-size: 1em;
  padding: 0.5em;
  margin: 0.5em;
  border-radius: 5px;
  width:400px;
  
  :focus {
    outline:none;
  }
`;

export const ButtonsFiltersWrapper = styled.div`
  display:flex;
  width:500px;
  justify-content: space-between;
`;